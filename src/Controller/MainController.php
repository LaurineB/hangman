<?php
/**
 * Created by PhpStorm.
 * User: nono
 * Date: 03/11/2017
 * Time: 11:54
 */

namespace App\Controller;

use App\Game\Game;
use App\Game\GameContext;
use App\Game\Loader\TextFileLoader;
use App\Game\Loader\XmlFileLoader;
use App\Game\WordList;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends Controller {
    /**
     * @Route("/")
     */
    public function index()
    {
        //dump(getenv(('APP_ENV')));
        //dump($this->get('logger'));
        return $this->render('main/index.html.twig');
    }

    /**
     * @Route("/admin/manageWordlist", name="manageWordList")
     *
     */
    public function manageWordlist()
    {
        $wordList = new WordList();
        $xmlFileLoader = new XmlFileLoader();
        //JE sais que ce n'est pas DIR mais je n'ai pas internet....
        $xmlFileLoader->load(__DIR__."../../data/words.xml");
        $txtFileLoader = new TextFileLoader();
        $txtFileLoader->load(__DIR__."../../data/words.txt");
        $wordList->addLoader('xml',$xmlFileLoader);
        $wordList->addLoader('txt',$txtFileLoader);

        return $this->render('admin/manageWordlist.html.twig', ['wordList' => $wordList]);
    }
}